def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, space_number):
    print_board(board)
    print(board[space_number], "has won")
    exit()

def top_row_is_winner(board):
    if board[0] == board[1] and board[1] == board[2]:
        return True
    else:
        return False

def middle_row_is_winner(board):
    if board[3] == board[4] and board[4] == board[5]:
        return True
    else:
        return False

def bottom_row_is_winner(board):
    if board[6] == board[7] and board[7] == board[8]:
        return True
    else:
        return False

def left_column_is_winner(board):
    if board[0] == board[3] and board[3] == board[6]:
        return True
    else:
        return False

def middle_column_is_winner(board):
    if board[1] == board[4] and board[4] == board[7]:
        return True
    else:
        return False

def right_column_is_winner(board):
    if board[2] == board[5] and board[5] == board[8]:
        return True
    else:
        return False

def left_diagonal_is_winner(board):
    if board[0] == board[4] and board[4] == board[8]:
        return True
    else:
        return False

def right_diagonal_is_winner(board):
    if board[2] == board[4] and board[4] == board[6]:
        return True
    else:
        return False

def is_row_winner(board, space_number):

    if space_number >= 1 and space_number <= 3:
        beginning_index = 0

    elif space_number >= 4 and space_number <= 6:
        beginning_index = 3

    else:
        beginning_index = 6

    if board[beginning_index] == board[beginning_index + 1] and board[beginning_index + 1] == board[beginning_index + 2]:
        return True

    else:
        return False



board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, space_number):
        game_over(board, space_number)

    elif is_row_winner(board, space_number):
        game_over(board, space_number)

    elif is_row_winner(board, space_number):
        game_over(board, space_number)

    elif left_column_is_winner(board):
        game_over(board, space_number)

    elif middle_column_is_winner(board):
        game_over(board, space_number)

    elif right_column_is_winner(board):
        game_over(board, space_number)

    elif left_diagonal_is_winner(board):
        game_over(board, space_number)

    elif right_diagonal_is_winner(board):
        game_over(board, space_number)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
